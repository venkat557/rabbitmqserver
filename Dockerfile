FROM rabbitmq:3.9.5-management

WORKDIR /


COPY . .
# RUN apt-get update && apt-get -y upgrade
# RUN apt-get install make automake gcc g++ subversion python3-dev
# RUN pip install --upgrade pip
# RUN pip install --upgrade setuptools
# ADD requirements.txt .
RUN ls -la
#RUN pip install grpcio==1.30.0
# RUN pip install --user -r requirements.txt
# RUN python --version
# RUN pip --version

# RUN pip install numpy --upgrade
# RUN pip install --user -r requirements.txt
# RUN apt-get install -y curl \
#   && curl -sL https://deb.nodesource.com/setup_9.x | bash - \
#   && apt-get install -y nodejs \
#   && curl -L https://www.npmjs.com/install.sh | sh
# RUN node --version
# RUN npm --version
# RUN npm install pm2 -g
#RUN pm2 status
# RUN pip install mysql-connector-python
# RUN pip install psycopg2-binary
# RUN pip install --upgrade google-api-core
# RUN pip install elasticsearch
# RUN pip install openpyxl



# RUN apt-get update
# RUN apt-get install erlang
# RUN apt-get install rabbitmq-server 
# RUN systemctl enable rabbitmq-server
# RUN systemctl start rabbitmq-server
# RUN systemctl status rabbitmq-server
# RUN rabbitmq-plugins enable rabbitmq_management


# RUN pip install fuzzywuzzy
# RUN pip install flashtext
#RUN apt-get install libomp-dev
# RUN apt-get install -y gcc make swig libomp-dev
# RUN pip install faiss-cpu
# RUN pip install swifter
# RUN pip install gcloud
# RUN pip install google-cloud-storage
# RUN pip install sqlalchemy-utils
# RUN pip install google-cloud-bigquery
# RUN pip install google-cloud-bigtable
# RUN pip install nltk
# RUN pip install pytest
# RUN GRPC_HEALTH_PROBE_VERSION=v0.3.1 && \
#     wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
#     chmod +x /bin/grpc_health_probe

EXPOSE 15672
# CMD ["python","server.py"]
#CMD ["pm2","start","server.py"]
#CMD ["sh", "-c", "tail -f /dev/null"]








